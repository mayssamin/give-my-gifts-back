﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {
	private LevelGenerator _levelGenerator;
	CameraManager _cameraManager;
	SpriteRenderer _spriteRenderer;
	public int _score = 1;
	// Use this for initialization
	void Start () {
		_levelGenerator = Camera.main.GetComponent<LevelGenerator> ();
		_cameraManager = Camera.main.GetComponent<CameraManager> ();
		_spriteRenderer = GetComponent<SpriteRenderer> ();
		transform.localScale = .8f * transform.localScale;
		_spriteRenderer.sortingOrder = (int)Layers.Layer_Gaming;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//transform.position = transform.position + new Vector3(0, _fallingSpeed,0);
		//transform.Rotate (new Vector3 (0,0,_rotationSpeed));
		
	}

	public void Dissapear (){
		Destroy (this.gameObject);
	}


}
