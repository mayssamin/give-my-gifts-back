﻿using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

public enum TileType{
	EMPTY,
	BARRIER,
	PLAT1,
	COLLECTIBLE1,
	COLLECTIBLE2,
	COLLECTIBLE3,
}
public class Tile{
	public TileType _tileType;
	public GameObject _gameObject;
	public int _row;
	public int _column;
	public bool _leftClose = true;
	public bool _topClose = true;
	public bool _bottomClose = true;
	public bool _rightClose = true;
	public Tile(){
		
	}
	public void Consume(){
		if (IsCollectible()) {
			_gameObject.GetComponent<Collectible> ().Dissapear ();
			_tileType = TileType.EMPTY;
		}
	}
	public bool IsCollectible(){
		return _tileType == TileType.COLLECTIBLE1 || _tileType == TileType.COLLECTIBLE2 || _tileType == TileType.COLLECTIBLE3;
	}
}
[System.Serializable]
public struct TileToObjectMap{
	[SerializeField]
	public TileType tileType;
	[SerializeField]
	public GameObject gameObject;
}
public class LevelGenerator : MonoBehaviour {
	public Vector2 _santaPosition = new Vector2( 1, 3 );
	public TileToObjectMap[] _tileGameObjects;
	private short _tilesInCamHeight = 1;

	private float _squareSize = 1;
	private const int _columnsCount = 8;
	private const int _startRow = 6;
	private int _rowsCount = 1000;
	private Tile[,] _tileMap;
	private Dictionary<TileType, GameObject> _tileToGameobjectMap;
	private List<List<GameObject>> _rowObjects;
	private List<List<int>> _rowEmptyColumns;
	private int _lastAddedRow = -1;
	private float _lastRowEndY = 0;

	//------ Collectibles variables --------
	private int _blockRowCountForCollectibles = 10;
	private int _numberOfCollectiblesInEachBlock = 3;
	private int _numberOfCollectibleTypes = 3;
	//--------------------------------------

	CameraManager cameraManager;
	Vector3 _firstTilePosition;
	// Use this for initialization
	void Start () {
		cameraManager = Camera.main.GetComponent<CameraManager> ();
		_squareSize =  cameraManager.GetCameraWidth() / ( _columnsCount - 1 );
		_tilesInCamHeight = (short) Mathf.CeilToInt( cameraManager.GetCameraHeight() / _squareSize );
		_tileToGameobjectMap = new Dictionary<TileType, GameObject> ();
		_rowObjects = new List<List<GameObject>> ();
		_rowEmptyColumns = new List<List<int>> (); 
		_lastRowEndY = cameraManager.GetCameraTop();
		_firstTilePosition = new Vector3 (cameraManager.GetCameraLeft () - _squareSize, _lastRowEndY, 0);
		foreach (TileToObjectMap tileMap in _tileGameObjects) {
			_tileToGameobjectMap.Add (tileMap.tileType, tileMap.gameObject);

		}

		GenerateMap ();
		DistributeCollectibles ();
		AddTilesToGameWorld ();

	}
	
	// Update is called once per frame
	void Update () {
		ManageTileObjects ();
	}
	void GenerateMap (){
		
		_tileMap = new Tile[_rowsCount,_columnsCount];
		// Initialize tiles
		for (int r = 0; r < _rowsCount; r++) {
			for (int c = 0; c < _columnsCount; c++) {
				_tileMap [r, c] = new Tile ();
				_tileMap [r, c]._row = r;
				_tileMap [r, c]._column = c;
			}
		}
		for (int r = 0; r < _rowsCount; r++) {
			for (int c = 0; c < _columnsCount; c++) {
				if (c == 0 || c == _columnsCount - 1) {
					_tileMap [r,c]._tileType = TileType.BARRIER;
				} else {
					if (r < _startRow) {
						//_tileMap [r, c]._tileType = TileType.EMPTY;
						setCellEmpty ( r,c);
					} else if (r == _startRow) {
						_tileMap [r, c]._tileType = TileType.PLAT1;


					} else {
						_tileMap [r, c]._tileType = TileType.PLAT1;
					}
				}
			}
		}

		// Generate a path
		int col1 = Random.Range(1, _columnsCount-1);
		int col2 = Random.Range(1, _columnsCount-1);
		setCellEmpty (_startRow, col1);
		setCellEmpty (_startRow, col2);
		int prevCol1, prevCol2;
		for (int r = _startRow + 1; r < _rowsCount; r++) {
			prevCol1 = col1;
			prevCol2 = col2;
			col1 += Random.Range(-1, 2);
			col2 += Random.Range(-1, 2);
			if (col1 == _columnsCount - 1)
				col1 -= 1;
			else if (col1 == 0)
				col1 += 1;
			if (col2 == _columnsCount - 1)
				col2 -= 1;
			else if (col2 == 0)
				col2 += 1;
			setCellEmpty (r, col1);
			setCellEmpty (r, col2);
			var cols = new List<int> ();
			cols.Add (col1);
			_rowEmptyColumns.Add (cols);
			if(col1 != col2){
				cols.Add (col2);
			}
			//open path
			if (col1 > prevCol1) {
				if (_tileMap [r-1, prevCol1 + 1]._tileType != TileType.EMPTY &&
				   _tileMap [r , prevCol1]._tileType != TileType.EMPTY)
					setCellEmpty( r , prevCol1);
				
			}
			else if (col1 < prevCol1) {
				if (_tileMap [r-1, prevCol1 - 1]._tileType != TileType.EMPTY &&
					_tileMap [r , prevCol1]._tileType != TileType.EMPTY)
					setCellEmpty(r , prevCol1);
			}
			if (col2 > prevCol2) {
				if (_tileMap [r-1, prevCol2 + 1]._tileType != TileType.EMPTY &&
					_tileMap [r , prevCol2]._tileType != TileType.EMPTY)
					setCellEmpty(r , prevCol2);
			}
			else if (col2 < prevCol2) {
				if (_tileMap [r-1, prevCol2 - 1]._tileType != TileType.EMPTY &&
					_tileMap [r , prevCol2]._tileType != TileType.EMPTY)
					setCellEmpty(r , prevCol2);
			}

		}
	}
	public void DistributeCollectibles (){
		
		for (int r = _startRow + 1; r < _rowsCount; r+=_blockRowCountForCollectibles) {

			int blockRows = _blockRowCountForCollectibles;
			int collectibleCount = _numberOfCollectiblesInEachBlock;
			if (r + blockRows > _rowsCount) {
				blockRows = _rowsCount - r;
				if (collectibleCount > blockRows)
					collectibleCount = blockRows;
			}
			var rowNumbers = new List<int>( );
			for (int i = 0; i < blockRows; i++) {
				rowNumbers.Add(i);
			}
			var randomRows = new int[collectibleCount];
			for (int i = 0; i < randomRows.Length; i++) {
				var thisNumber = Random.Range(0, rowNumbers.Count-1);
				randomRows[i] = rowNumbers[thisNumber];
				rowNumbers.RemoveAt(thisNumber);
			}

			//insert gift

			foreach( int row in randomRows){
				int collectibleType = Random.Range (1, _numberOfCollectibleTypes+1);

				//Debug.Log (r-(_startRow+1)+row);
				int absoluteRow = r-(_startRow+1)+row;
				int col = _rowEmptyColumns[ absoluteRow ][Random.Range(0, _rowEmptyColumns[absoluteRow].Count-1)];
				_tileMap [r+row , col]._tileType = (TileType)System.Enum.Parse( typeof(TileType), "COLLECTIBLE"+collectibleType);
			}
		}
	}
	public void AddTilesToGameWorld(  ){
		int startingRow = _lastAddedRow + 1;

		Vector3 currentTilePos =  new Vector3( _firstTilePosition.x, _lastRowEndY - _squareSize*.5f, 0 );
		int counter = 0;
		for (int r = startingRow; r < ( startingRow + _tilesInCamHeight * 2 ) && r < _rowsCount; r++) {
			List<GameObject> tiles = new List<GameObject> ();
			for (int c = 0; c < _columnsCount; c++) {
				currentTilePos.x += _squareSize;
				if (_tileToGameobjectMap.ContainsKey (_tileMap [r, c]._tileType)) {
					GameObject prefab = _tileToGameobjectMap [_tileMap [r, c]._tileType];

					GameObject tile = Instantiate (prefab.transform, 
						                 prefab.transform.position, 
						                 prefab.transform.rotation).gameObject;
					if (_tileMap [r, c]._tileType == TileType.PLAT1) {
						tile.GetComponent<Platform> ().SetType (_tileMap [r, c]._topClose, _tileMap [r, c]._leftClose, 
							_tileMap [r, c]._bottomClose, _tileMap [r, c]._rightClose);
					}
					SpriteRenderer spriteRenderer = tile.GetComponent<SpriteRenderer> ();

					Vector2 scale = tile.transform.localScale;

					scale *= _squareSize / spriteRenderer.bounds.size.x;

					tile.transform.localScale = scale;
				 
					tile.transform.position = currentTilePos;
					spriteRenderer.sortingOrder = (int)Layers.Layer_Gaming;
					tiles.Add (tile);
					_tileMap [r, c]._gameObject = tile;
				}
			}
			counter++;
			currentTilePos.y -= _squareSize;
			currentTilePos.x = _firstTilePosition.x; 
			_rowObjects.Add (tiles);
		}
		_lastAddedRow = startingRow + _tilesInCamHeight * 2 -1;
		_lastRowEndY -= counter * _squareSize;
	}
	private void setCellEmpty(int row, int col){
		_tileMap [row, col]._tileType = TileType.EMPTY;
		//if(_tileMap [row - 1, col]._tileType == TileType.PLAT1)

		if(row > 0)	
			_tileMap [row - 1, col]._bottomClose = false;
		if(row+1<_rowsCount) //&& _tileMap [row + 1, col]._tileType == TileType.PLAT1)
			_tileMap [row + 1, col]._topClose = false;
		//if(_tileMap [row, col -1]._tileType == TileType.PLAT1)
			_tileMap [row, col -1]._rightClose = false;
		//if(_tileMap [row, col+1]._tileType == TileType.PLAT1)
			_tileMap [row, col+1]._leftClose = false;

	}
	private void ManageTileObjects (){
		//Remove visited out of view tiles
		while( _rowObjects.Count > 0 && _rowObjects [0].Count > 0 && 
			(_rowObjects [0] [0].transform.position.y - _squareSize*.5) > cameraManager.GetCameraTop()){
			foreach (GameObject tile in _rowObjects [0]) {
				Destroy (tile);
			}
			_rowObjects.RemoveAt (0);
		}

		//Add new tiles
		if (_rowObjects.Count > 0 && _lastAddedRow < _rowsCount) {
			if (_rowObjects [_rowObjects.Count-1] [0].transform.position.y > cameraManager.GetCameraBottom() * 2) {
				AddTilesToGameWorld ();
			}
		}
	} 

	public float GetSquareSize(){
		return _squareSize;
	}
	public Vector2 GetPlayerInitialTile(){
		return _santaPosition;
	}
	public bool IsMovable( float row, float col, ref Tile tile){
		bool result = row > -1 && row < _rowsCount && col > -1 && col < _columnsCount;

		tile = _tileMap [(int)row, (int)col];
		result = result && (_tileMap [(int)row, (int)col]._tileType == TileType.EMPTY ||
			_tileMap [(int)row, (int)col]._tileType == TileType.COLLECTIBLE1 ||
			_tileMap [(int)row, (int)col]._tileType == TileType.COLLECTIBLE2 ||
			_tileMap [(int)row, (int)col]._tileType == TileType.COLLECTIBLE3 );
		return result;
	}
//	public bool IsMovable( Vector3 pos ){
//		int row = Mathf.FloorToInt ((_firstTilePosition.y - _squareSize*.5f - pos.y) / _squareSize);
//		int col = Mathf.FloorToInt ((pos.x - _squareSize*.5f - _firstTilePosition.x) / _squareSize);
//
//		return IsMovable(row, col);
//	}
	public Tile GetTile( Vector3 pos){
		int row = Mathf.FloorToInt ((_firstTilePosition.y - _squareSize*.5f - pos.y) / _squareSize);
		int col = Mathf.FloorToInt ((pos.x - _squareSize*.5f - _firstTilePosition.x) / _squareSize);
		//Debug.Log (row+","+col);
		return _tileMap [row, col];
	}


}
