﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
	public Sprite[] _platforms;
	SpriteRenderer _spriteRenderer;
	int _type = 0;
	// Use this for initialization
	void Start () {
		_spriteRenderer = GetComponent<SpriteRenderer> ();
		_spriteRenderer.sprite = _platforms [_type];
	}
	public void SetType( bool topClose, bool leftClose, bool bottomClose, bool rightClose){
		 _type = BoolToType( topClose, leftClose, bottomClose, rightClose);
		if (GetComponent<SpriteRenderer> () != null) {
			GetComponent<SpriteRenderer> ().sprite = _platforms [_type];
		}
	}
	private int BoolToType(bool topClose, bool leftClose, bool bottomClose, bool rightClose ){
		int result = 0;
		result += topClose ? 8 : 0;
		result += leftClose ? 4 : 0;
		result += bottomClose ? 2 : 0;
		result += rightClose ? 1 : 0;
		return result;
	}
}
