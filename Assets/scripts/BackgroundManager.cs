﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Layers{
	Layer_BackgroundBack ,
	Layer_BackgroundMiddle,
	Layer_BackgroundFront,
	Layer_GamingBack,
	Layer_Gaming,
	Layer_ForegroundBack,
	Layer_ForegroundMiddle,
	Layer_ForegroundFront
}

public class BackgroundManager : MonoBehaviour {
	public GameObject _mainBackgroundLayers;
	public List<GameObject> _otherBackgroundLayers;
	private CameraManager _cameraManager;
	private float _backgroundScale = 1;
	// Use this for initialization
	void Start () {
		_cameraManager = Camera.main.GetComponent<CameraManager> ();
		GameObject background = Instantiate (_mainBackgroundLayers.transform, 
			_mainBackgroundLayers.transform.position, 
			_mainBackgroundLayers.transform.rotation).gameObject;
		_backgroundScale =  _cameraManager.GetCameraWidth ()/background.GetComponent<BackgroundLayer> ().GetImageWidth() ;
		background.GetComponent<BackgroundLayer> ().widthScale *= _backgroundScale;


		for (int index = _otherBackgroundLayers.Count - 1; index >= 0; index--) {
			background = Instantiate (_otherBackgroundLayers[index].transform, 
				_otherBackgroundLayers [index].transform.position, 
				_otherBackgroundLayers [index].transform.rotation).gameObject;
			background.GetComponent<BackgroundLayer> ().widthScale *= _backgroundScale;

		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		
	}

	public float BackgroundScale {
		get {
			return _backgroundScale;
		}
	}
}
