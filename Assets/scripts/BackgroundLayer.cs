﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectAlign{
	OA_LEFT,
	OA_RIGHT,
	OA_CENTER
}
public enum ScaleType{
	ST_AUTO,
	ST_MANUAL
}
public class BackgroundLayer : MonoBehaviour {
	public Layers _ZOrder = 0;
	public float _speedCoef = 1;
	public float _initialVerticalOffset = 0;
	public ObjectAlign _Align = ObjectAlign.OA_CENTER;
	public float _widthScale = 1;
	public ScaleType _scaleType = ScaleType.ST_AUTO;
	public List<GameObject> _UsableBGs;
	private List<GameObject> _currentBGs;
	private CameraManager _cameraManager;
	private float _fillBottom;
	Vector3 _previousCameraPosition;
	// Use this for initialization
	void Start () {
		_cameraManager = Camera.main.GetComponent<CameraManager> ();
		_currentBGs = new List<GameObject> ();
		_previousCameraPosition = Camera.main.transform.position;
		_fillBottom = _cameraManager.GetCameraTop ()+_initialVerticalOffset;
	}

	public float GetImageWidth(){
		return _UsableBGs [0].GetComponent<SpriteRenderer> ().bounds.size.x;
	}

	void FixedUpdate () {
		
			float camSpeed = Camera.main.transform.position.y - _previousCameraPosition.y;
			_previousCameraPosition = Camera.main.transform.position;
			Vector3 parallaxOffset = new Vector3( 0.0f, camSpeed - _speedCoef*camSpeed,0);
			_fillBottom += parallaxOffset.y;

			for( int i = _currentBGs.Count-1; i>=0;i--  ){
				GameObject background = _currentBGs [i].gameObject;
				//Update position to create parallax

				background.transform.position = background.transform.position + parallaxOffset; 



				//Remove out of view images

				float bgHeight = background.GetComponent<SpriteRenderer> ().bounds.size.y;
			if (background.transform.position.y - bgHeight * .5 > _cameraManager.GetCameraTop()) {
					_currentBGs.RemoveAt (i);
					Destroy (background);
				}
			}

		while (_fillBottom > _cameraManager.GetCameraBottom ()-1) {
				int index = Random.Range(0, _UsableBGs.Count);
				GameObject background = Instantiate (_UsableBGs[index].transform, 
					_UsableBGs [index].transform.position, 
					_UsableBGs [index].transform.rotation).gameObject;
				_currentBGs.Add (background);

				SpriteRenderer spriteRenderer = background.GetComponent<SpriteRenderer> ();

				Vector2 scale = background.transform.localScale;
				if (_scaleType == ScaleType.ST_AUTO) {
					scale *= _widthScale;
				}
				 


				background.transform.localScale = scale;
				Vector2 spriteSize = spriteRenderer.bounds.size; 

			_cameraManager.AlignObject (background, spriteRenderer.bounds, _Align);
				float shift = Random.Range (background.GetComponent<BackgroundImage>()._MinDistanceBetweenBGs, 
					background.GetComponent<BackgroundImage>()._MaxDistanceBetweenBGs);
				background.transform.position =  new Vector2( background.transform.position.x, _fillBottom - spriteSize.y*.5f - shift) ;
				spriteRenderer.sortingOrder = (int) _ZOrder;
				_fillBottom -=spriteRenderer.bounds.size.y + shift;


			}


	}

	public float widthScale {
		get {
			return _widthScale;
		}
		set{
			_widthScale = value;	
		}
	}
}
