﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class main : MonoBehaviour {
	public Image _logo;
	public Text _brand;
	private int _waitTime = 1;
	private bool _startFading = false;
	private float _fadeSpeed = .8f;
	// Use this for initialization
	void Start () {
		StartCoroutine(workAfterSeconds());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit();
		if (_startFading) {
			_logo.color = Color.Lerp (_logo.color, Color.black, Time.deltaTime*_fadeSpeed);
			_brand.color = Color.Lerp (_brand.color, Color.black, Time.deltaTime*_fadeSpeed);
			if (_logo.color.r < .02f && _logo.color.g < .02f && _logo.color.b < .02f) {
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
			}
		}
	}
	IEnumerator workAfterSeconds() {
		yield return new WaitForSeconds(_waitTime);
		_startFading = true;

	}
}
