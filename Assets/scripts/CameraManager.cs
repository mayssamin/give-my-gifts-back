﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
	private Vector3 _cameraBotRight, _cameraTopLeft;
	private float _camOffset;
	private float _cameraHeight = 0;
	private Vector2 _cameraSize;
	private GameObject _player;
	// Use this for initialization
	void Start () {
		ComputeScreenEdges ();
		_player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		FollowPlayer ();
		updateCameraInfo ();
		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit();
	}

	private void ComputeScreenEdges(){
		updateCameraInfo ();
		_cameraHeight = _cameraTopLeft.y - _cameraBotRight.y;
		_cameraSize = new Vector2 (_cameraBotRight.x-_cameraTopLeft.x  , _cameraHeight);

	}
	void updateCameraInfo(){
		_cameraBotRight = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, 0, _camOffset));
		_cameraTopLeft = Camera.main.ScreenToWorldPoint (new Vector3 (0, Screen.height, _camOffset));
	}
	public float GetCameraRight(){

		return _cameraBotRight.x;
	}
	public float GetCameraLeft(){

		return _cameraTopLeft.x;
	}
	public float GetCameraTop(){

		return _cameraTopLeft.y;
	}
	public float GetCameraBottom(){

		return _cameraBotRight.y;
	}
	public float GetCameraCenterX(){

		return _cameraBotRight.x-_cameraSize.x*.5f;
	}

	public  float GetCameraWidth(){
		return _cameraSize.x;
	}

	public  float GetCameraHeight(){
		return _cameraSize.y;
	}

	public void AlignObject( GameObject gameObject,Bounds bounds, ObjectAlign objectAlign){
		float x = 0;
		if (objectAlign == ObjectAlign.OA_LEFT) {

			x = GetCameraLeft() + bounds.size.x*.5f;
		}
		else if (objectAlign == ObjectAlign.OA_RIGHT) {
			x = GetCameraRight() - bounds.size.x*.5f;
		}
		else {
			x = GetCameraCenterX()  ;
		}
		gameObject.transform.position = new Vector2 (x, gameObject.transform.position.y);
	}
	public void AlignToBottomOf( GameObject gameObject,Bounds gameObjectBounds, Bounds targetBounds){
		gameObject.transform.position=new Vector2(gameObject.transform.position.x,
			targetBounds.center.y - .5f*( targetBounds.size.y+gameObjectBounds.size.y));
	}

	public void ScaleToFillWidth( GameObject gameObject, Bounds bounds){

		gameObject.transform.localScale *= _cameraSize.x / bounds.size.x;
	}

	public void FollowPlayer(){
		transform.position = new Vector3 (transform.position.x, _player.transform.position.y, transform.position.z);

	}
}
