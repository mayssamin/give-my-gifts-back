﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerManager2 : MonoBehaviour {
	private LevelGenerator _levelGenerator;
	CameraManager _cameraManager;
	private BoxCollider2D _boxCollider;

	float _verticalSpeed = 3; //how many tiles per second
	float _newPositionX = 0;
	Vector2 _currentTile;
	private int _score = 0;
	public Text _scoreText;
	//======== variables for horizontal move =======
	float prevX=0;
	float prevXTime = 0;
	float _prevHorizontalDir = 0;
	private float _timeToNextDest = 1;
	float _horizontalSpeed = 6;	//how many tiles per second
	float _offsetHorizontalMoves = .5f;
	//==============================================
	// Use this for initialization
	void Start () {
		_levelGenerator = Camera.main.GetComponent<LevelGenerator> ();
		_cameraManager = Camera.main.GetComponent<CameraManager> ();
		_boxCollider = GetComponent<BoxCollider2D> ();
		float scale = .96f * _levelGenerator.GetSquareSize()/_boxCollider.bounds.size.x;
		transform.localScale = new Vector3 (scale, scale, scale);
		_scoreText.rectTransform.localPosition = new Vector3( -Screen.width*.5f+50,Screen.height*.5f-50,0);
		_scoreText.text = ""+_score;
		_currentTile = _levelGenerator.GetPlayerInitialTile ();
		UpdatePositionToCurrentTile ();
		_newPositionX = transform.position.x;
		prevX = transform.position.x;
		prevXTime = Time.time;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		MoveDown ();

		foreach ( Touch touch in Input.touches){
			if (touch.phase == TouchPhase.Began) {
				if (touch.position.x > Screen.width * .5) {
					MoveOneBlock (1);
				} else {
					MoveOneBlock (-1);
				}
			}
		}
		if(Input.GetKeyUp(KeyCode.RightArrow))
			MoveOneBlock (1);
		else if(Input.GetKeyUp(KeyCode.LeftArrow))
			MoveOneBlock (-1);
		

		//Update horizontal position

		float timePassed = Time.time - prevXTime;
		float currentX = Mathf.Lerp( prevX, _newPositionX, timePassed /_timeToNextDest );
		if (currentX > _cameraManager.GetCameraLeft () && currentX < _cameraManager.GetCameraRight ()) {

			transform.position = new Vector3( currentX, transform.position.y, transform.position.z);
		}
		_scoreText.text = ""+_score;

	}
	void UpdatePositionToCurrentTile(){
		transform.position = new Vector3 ( _cameraManager.GetCameraLeft() + _currentTile.x*_levelGenerator.GetSquareSize(), 
			_cameraManager.GetCameraTop() - _currentTile.y * _levelGenerator.GetSquareSize());
		
	}
	void MoveOneBlock( int pace ){
		if (Mathf.Abs( transform.position.x - _newPositionX )<_levelGenerator.GetSquareSize()*_offsetHorizontalMoves ||
			_prevHorizontalDir != pace) {
			//if (Time.time - _lastMove > _delayBetweenHorizontalMoves) {
			//_lastMove = Time.time;
			float newCol = _currentTile.x + pace;
			//Debug.Log ("new x "+newCol);
			Tile nextTile = null;
			if (_levelGenerator.IsMovable (_currentTile.y, newCol, ref nextTile)) {
				_currentTile.x = newCol;
				prevX = transform.position.x;
				prevXTime = Time.time;
				_newPositionX = _cameraManager.GetCameraLeft () + _currentTile.x * _levelGenerator.GetSquareSize ();
				float distBlocks = Mathf.Abs(prevX - _newPositionX)/_levelGenerator.GetSquareSize();
				_timeToNextDest = distBlocks / _horizontalSpeed;
				CheckCollision (nextTile);
			} 
			_prevHorizontalDir = pace;
		}
	}

	void MoveDown(){
		float newPosY =  transform.position.y - _verticalSpeed*_levelGenerator.GetSquareSize()*Time.deltaTime;
		//Debug.Log ("" + _boxCollider.bounds.size.y +", "+ _boxCollider.offset.y);
		float newBottom = newPosY -	_boxCollider.bounds.size.y + _boxCollider.offset.y* transform.localScale.x;
		Tile tile = _levelGenerator.GetTile (new Vector3 (_newPositionX, newBottom, transform.position.z));
		if (_levelGenerator.IsMovable (tile._row, tile._column, ref tile)) {
			transform.position = new Vector3 (transform.position.x, newPosY, transform.position.z);
			_currentTile.y = tile._row;
		} 
		CheckCollision (tile);

	}

	void CheckCollision( Tile tile ){
		if (tile._tileType != TileType.EMPTY) {
			if (tile.IsCollectible ()) {
				Collectible collectible = tile._gameObject.GetComponent<Collectible> ();
				_score += collectible._score;

				tile.Consume ();
			}

		}
	}
}
